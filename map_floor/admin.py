from leaflet.admin import LeafletGeoAdmin
from django.contrib import admin

from . import models as floor_models


admin.site.register(floor_models.FloorMap, LeafletGeoAdmin)
