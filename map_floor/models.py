from djgeojson.fields import PolygonField
from django.db import models


class FloorMap(models.Model):

    title = models.CharField(max_length=256)
    description = models.TextField()
    picture = "/static/images/floor_plan.jpg" # //models.ImageField()
    geom = PolygonField()

    def __str__(self):
        return self.title

    @property
    def picture_url(self):
        return self.picture.url
